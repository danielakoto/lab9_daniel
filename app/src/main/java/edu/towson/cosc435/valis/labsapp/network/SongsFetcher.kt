package edu.towson.cosc435.valis.labsapp.network

import android.graphics.Bitmap
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import edu.towson.cosc435.valis.labsapp.model.Song
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response

interface ISongsFetcher {
    suspend fun fetchSongs(): List<Song>
    suspend fun fetchIcon(url: String): Bitmap?
}

// TODO - 8. Implement ISongsFetcher. Fetch the Songs list from the given URL.
class SongsFetcher : ISongsFetcher {
    private val URL = "https://my-json-server.typicode.com/rvalis-towson/lab_api/songs"
    override suspend fun fetchSongs(): List<Song> {
        return withContext(Dispatchers.IO) {
            val client = OkHttpClient()
            val request = Request.Builder()
                .get()
                .url(URL)
                .build()
            val response: Response = client.newCall(request).execute()
            val body: String? = response.body?.string()

            val listItemType = object: TypeToken<List<Song>>() {}.type
            val songs = Gson().fromJson<List<Song>>(body, listItemType)
            songs
        }
    }

    override suspend fun fetchIcon(url: String): Bitmap? {
        TODO("Not yet implemented")
    }
}

//data class ApiResult()