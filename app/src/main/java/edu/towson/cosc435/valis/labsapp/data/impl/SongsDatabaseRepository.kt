package edu.towson.cosc435.valis.labsapp.data.impl

import android.app.Application
import androidx.room.Room
import edu.towson.cosc435.valis.labsapp.data.ISongsRepository
import edu.towson.cosc435.valis.labsapp.data.SongsDatabase
import edu.towson.cosc435.valis.labsapp.model.Song

class SongsDatabaseRepository(app: Application) : ISongsRepository {
    private val db: SongsDatabase
    private var _songs: List<Song> = listOf()

    init {
        db = Room.databaseBuilder(
            app,
            SongsDatabase::class.java,
            "songs.db"
        ).fallbackToDestructiveMigration().build()
    }

    override suspend fun getSongs(): List<Song> {
        return db.songDao().getSongs()
    }

    override suspend fun deleteSong(song: Song) {
        db.songDao().deleteSong(song)
    }

    override suspend fun addSong(song: Song) {
        db.songDao().addSong(song)
    }

    override suspend fun toggleAwesome(song: Song) {
        val newSong = song.copy(isAwesome = !song.isAwesome)
        db.songDao().updateSong(newSong)
    }
}