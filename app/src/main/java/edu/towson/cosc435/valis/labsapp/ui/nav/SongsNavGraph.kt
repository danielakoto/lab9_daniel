package edu.towson.cosc435.valis.labsapp.ui.nav

import androidx.activity.viewModels
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.compose.runtime.getValue
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import edu.towson.cosc435.valis.labsapp.model.Song
import edu.towson.cosc435.valis.labsapp.ui.confirmdialog.ConfirmViewModel
import edu.towson.cosc435.valis.labsapp.ui.newsong.NewSongView
import edu.towson.cosc435.valis.labsapp.ui.newsong.NewSongViewModel
import edu.towson.cosc435.valis.labsapp.ui.songlist.SongListView
import edu.towson.cosc435.valis.labsapp.ui.songlist.SongListViewModel

@ExperimentalComposeUiApi
@ExperimentalFoundationApi
@Composable
fun SongsNavGraph(
    navController: NavHostController = rememberNavController()
) {
    val vm: SongListViewModel = viewModel()
    NavHost(
        navController = navController,
        startDestination = Routes.SongList.route
    ) {
        composable(Routes.SongList.route) {
            SongListScreen(vm)
        }
        composable(Routes.NewSong.route) {
            val newSongViewModel: NewSongViewModel = viewModel()
            NewSongView(
                newSongViewModel,
                onAddSong = { song ->
                    vm.addSong(song)
                    navController.navigate(Routes.SongList.route) {
                        popUpTo(Routes.SongList.route)
                    }
                }
            )
        }
    }
}

@ExperimentalFoundationApi
@Composable
fun SongListScreen(
    vm: SongListViewModel
) {
    val confirmViewModel: ConfirmViewModel = viewModel()
    val songs by vm.songs
    val selectedSong by vm.selectedSong
    SongListView(
        songs,
        selectedSong,
        confirmViewModel,
        waiting=vm.waiting.value,
        waitingProgress=vm.waitingProgress.value,
        onDelete=vm::deleteSongAsync,
        onToggle=vm::toggleAwesome,
        onFilter=vm::filter,
        onSelectSong=vm::selectSong
    )
}