package edu.towson.cosc435.valis.labsapp.data.impl

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import edu.towson.cosc435.valis.labsapp.data.ISongsRepository
import edu.towson.cosc435.valis.labsapp.model.Song
import kotlinx.coroutines.delay
import java.util.*

class SongsMemoryRepository : ISongsRepository {

    private var _songs = listOf<Song>()

    init {
        _songs = (0..20).map { i ->
            Song(UUID.randomUUID().toString(), "Song $i", "Artist $i", i, i % 3 == 0, null)
        }
    }

    override suspend fun getSongs(): List<Song> {
        return _songs
    }

    override suspend fun deleteSong(song: Song) {
        delay(5000)
        val idx = _songs.indexOf(song)
        _songs = _songs.subList(0, idx) + _songs.subList(idx+1, _songs.size)
    }

    override suspend fun addSong(song: Song) {
//        delay(5000)
        _songs = listOf(song) + _songs
    }

    override suspend fun toggleAwesome(song: Song) {
        val newSong = song.copy(isAwesome = !song.isAwesome)
        val idx = _songs.indexOf(song)
        _songs = _songs.subList(0, idx) + listOf(newSong) + _songs.subList(idx+1, _songs.size)
    }
}