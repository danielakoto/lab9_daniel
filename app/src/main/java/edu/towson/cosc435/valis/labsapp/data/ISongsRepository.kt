package edu.towson.cosc435.valis.labsapp.data

import androidx.lifecycle.LiveData
import edu.towson.cosc435.valis.labsapp.model.Song

interface ISongsRepository {
    suspend fun getSongs(): List<Song>
    suspend fun deleteSong(song: Song)
    suspend fun addSong(song: Song)
    suspend fun toggleAwesome(song: Song)
}