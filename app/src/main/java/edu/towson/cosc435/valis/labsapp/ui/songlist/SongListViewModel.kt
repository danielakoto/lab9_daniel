package edu.towson.cosc435.valis.labsapp.ui.songlist

import android.app.Application
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import edu.towson.cosc435.valis.labsapp.data.ISongsRepository
import edu.towson.cosc435.valis.labsapp.data.SongsDatabase
import edu.towson.cosc435.valis.labsapp.data.impl.SongsApiWithDatabase
import edu.towson.cosc435.valis.labsapp.data.impl.SongsDatabaseRepository
import edu.towson.cosc435.valis.labsapp.model.Song
import edu.towson.cosc435.valis.labsapp.network.SongsFetcher
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SongListViewModel(app: Application) : AndroidViewModel(app) {
    private val _songs: MutableState<List<Song>> = mutableStateOf(listOf())
    val songs: State<List<Song>> = _songs

    private val _selected: MutableState<Song?>
    val selectedSong: State<Song?>

    private val _waiting: MutableState<Boolean>
    val waiting: State<Boolean>

    private val _waitingProgress: MutableState<Float>
    val waitingProgress: State<Float>

    // TODO - 9. Replace with SongsApiWithDatabase class
    // private val _repository: ISongsRepository = SongsDatabaseRepository(app)
    private val _repository: ISongsRepository =
        SongsApiWithDatabase(
            songsDatabase = SongsDatabaseRepository(app),
            songsFetcher = SongsFetcher()
        )

    init {
        viewModelScope.launch {
            _songs.value = _repository.getSongs()
        }
        _selected = mutableStateOf(null)
        selectedSong = _selected
        _waiting = mutableStateOf(false)
        waiting = _waiting
        _waitingProgress = mutableStateOf(0.0f)
        waitingProgress = _waitingProgress
    }

    fun addSong(song: Song) {
        viewModelScope.launch {
            _waiting.value = true
            _repository.addSong(song)
            _songs.value = _repository.getSongs()
            _waiting.value = false
        }
    }

    fun deleteSong(song: Song) {
        viewModelScope.launch {
            deleteSongAsync(song)
        }
    }

    suspend fun deleteSongAsync(song: Song) {
        _waiting.value = true
        _waitingProgress.value = 0.0f
        val progJob = viewModelScope.async { _incrementProgress() }
        val deleteJob = viewModelScope.async { _repository.deleteSong(song) }
        progJob.start()
        deleteJob.await()
        progJob.cancel()
        _songs.value = _repository.getSongs()
        _waiting.value = false
        _waitingProgress.value = 0.0f
    }

    suspend fun _incrementProgress() {
        while(true) {
            delay(500)
            _waitingProgress.value += (1.0f / 10.0f)
        }
    }

    fun toggleAwesome(song: Song) {
        viewModelScope.launch {
            _repository.toggleAwesome(song)
            _songs.value = _repository.getSongs()
        }
    }

    fun filter(search: String) {
        viewModelScope.launch {
            _songs.value = _repository.getSongs().filter { a -> a.name.contains(search, true) }
//            _songs.value = _songs.value.filter { a -> a.name.contains(search, true) }
        }
    }

    fun selectSong(song: Song) {
        _selected.value = song
    }
}